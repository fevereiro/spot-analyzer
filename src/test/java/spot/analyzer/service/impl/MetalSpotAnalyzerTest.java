package spot.analyzer.service.impl;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import spot.analyzer.dto.SpotsResultDTO;
import spot.analyzer.exception.InvalidMatrixFormatException;
import spot.analyzer.service.SpotsAnalyzer;

public class MetalSpotAnalyzerTest {
	
	private SpotsAnalyzer spotAnalyzer;
	
	@Before
	public void setSpotAnalyzer() {
		this.spotAnalyzer = new MetalSpotsAnalyzer();
	}
	
	@Test
	public void matrixWithEmptySpotsMustReturnEmptyParameters() {
		int[][] matrixWithEmptySpots = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
		
		SpotsResultDTO result = spotAnalyzer.execute(matrixWithEmptySpots);
		
    	assertTrue(result.getTotalArea() == 0 && result.getNumberOfSpots() == 0 
    			&& result.getAverageSpotArea() == 0 && result.getTotalArea() == 0);
	}
	
	
	@Test
	public void matrixWithSpotsMustReturnFilledParameters() {
		int[][] matrixWithEmptySpots = {{0,1,1,0},{0,0,1,1},{0,0,0,0},{1,1,0,0}};
		
		SpotsResultDTO result = spotAnalyzer.execute(matrixWithEmptySpots);
    	assertTrue(result.getTotalArea() == 6 && result.getNumberOfSpots() == 2 
    			&& result.getAverageSpotArea() == 3.0 && result.getTotalArea() == 6);
	}
	
	
	@Test(expected = InvalidMatrixFormatException.class)
	public void matrixWithNotAllowedValuesMustThrownException() {
		int[][] matrixWithEmptySpots = {{0,-1,7,0},{0,0,2,4},{0,0,0,0},{1,1,0,0}};
		
		spotAnalyzer.execute(matrixWithEmptySpots);
	}
	
	@Test(expected = InvalidMatrixFormatException.class)
	public void matrixWithNotAllowedDimensionsMustThrownException() {
		int[][] matrixWithEmptySpots = {{0,1,1,0},{0,0,1,1},{0,0,0,0},{1,1,0,0,1,0,1}};
		
		spotAnalyzer.execute(matrixWithEmptySpots);
	}

}
