package spot.analyzer.controller;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import spot.analyzer.dto.SpotsResultDTO;
import spot.analyzer.service.SpotsAnalyzer;
import spot.analyzer.service.impl.MetalSpotsAnalyzer;

@RequestScoped
@Path("/")
public class SpotAnalyzerController {
	
	@POST
	@Path("spot_check")
    @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    public Response spotAnalyze(int[][] matrix) {
		SpotsAnalyzer spotAnalyzer = new MetalSpotsAnalyzer();
    	SpotsResultDTO result = spotAnalyzer.execute(matrix);
    	return Response
    			.status(Response.Status.OK)
    			.entity(result.toJson())
    			.build();
    }
}
