package spot.analyzer.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InvalidMatrixFormatMapper implements ExceptionMapper<InvalidMatrixFormatException> {

	@Override
	public Response toResponse(InvalidMatrixFormatException exception) {
		return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).entity("Invalid Matrix Format")
				.type("text/plain").build();
	}
}
