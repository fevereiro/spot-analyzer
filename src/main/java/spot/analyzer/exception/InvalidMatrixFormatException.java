package spot.analyzer.exception;

public class InvalidMatrixFormatException extends RuntimeException {

	private static final long serialVersionUID = 6908839114715713733L;

	public InvalidMatrixFormatException() {
		super();
	}

}
