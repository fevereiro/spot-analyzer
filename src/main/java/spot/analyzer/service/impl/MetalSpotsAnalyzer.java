package spot.analyzer.service.impl;

import javax.xml.ws.Holder;

import spot.analyzer.dto.SpotsResultDTO;
import spot.analyzer.exception.InvalidMatrixFormatException;
import spot.analyzer.service.SpotsAnalyzer;
import spot.analyzer.util.DirectionsType;
import spot.analyzer.util.MatrixUtil;

public class MetalSpotsAnalyzer implements SpotsAnalyzer {
	
	int biggestSpotArea;
	int numberOfSpots;
	int totalSpotArea;
	
	@Override
	public SpotsResultDTO execute(int [][] matrix) {
		if (!MatrixUtil.isValidMatrix(matrix)) {
			throw new InvalidMatrixFormatException();
		}
		
        for (int row = 0; row < matrix.length; row++) {
            for (int column= 0; column < matrix[0].length; column++) {
                if (matrix[row][column] == 1) {
                	numberOfSpots++;
                    setBiggestSpotArea(findBiggestSpot(matrix, row, column));
                }
            }
        }
		
		return getResult();
	}
	
    private int findBiggestSpot(int [][] grid, int... directions) {
    	int row = directions[0];
    	int column = directions[1];
    	
    	if (MatrixUtil.hasCellZeroValue(grid, row, column)) {
    		return 0;
    	}
    	
    	MatrixUtil.resetSpotValue(grid, row, column);
        return searchSpotAccordingToDirections(grid, row, column);
    }
    
    private int searchSpotAccordingToDirections(int [][] grid, int row, int col) {
    	Holder<Integer> biggestAux = new Holder<>(1);
    	DirectionsType.stream()
    		.forEach(direction -> 
	    		biggestAux.value += findBiggestSpot(grid, direction.calculateMove(row, col))
    		);
    	totalSpotArea++;
    	return biggestAux.value;
    }
	
	private double getAverageSpotArea() {
		return totalSpotArea != 0 && numberOfSpots != 0 ? totalSpotArea / numberOfSpots : 0d;
	}
	
	private void setBiggestSpotArea(int actualSize) {
		this.biggestSpotArea = Math.max(this.biggestSpotArea, actualSize);
	}
	
	private SpotsResultDTO getResult() {
		return new SpotsResultDTO.Builder().setBiggestArea(biggestSpotArea)
		.setNumberOfSpots(numberOfSpots).setTotalArea(totalSpotArea)
		.setAverageArea(getAverageSpotArea()).build();
	}

}
