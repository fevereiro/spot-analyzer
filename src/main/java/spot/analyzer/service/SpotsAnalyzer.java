package spot.analyzer.service;

import spot.analyzer.dto.SpotsResultDTO;

public interface SpotsAnalyzer {
	
	public SpotsResultDTO execute(int [][] matrix);

}
