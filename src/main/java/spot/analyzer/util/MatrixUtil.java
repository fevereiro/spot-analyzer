package spot.analyzer.util;

import java.util.Arrays;
import java.util.stream.IntStream;

public final class MatrixUtil {

   private MatrixUtil() {
	   throw new UnsupportedOperationException("Utility class cannot be instantiated");
   }
   
   public static boolean isValidMatrix(int[][] matrix) {
	   if (matrix == null) {
		   return false;
	   }
	   
	   for (int iterator = 0, actualLength = matrix.length; iterator < actualLength; iterator++) {
		   if (matrix[iterator].length != actualLength) {
		     return false;
		   }
		 }
	   
	   return Arrays.stream(matrix)
			   .flatMapToInt(Arrays::stream)
			   .allMatch(arrayValue -> arrayValue == 0 || arrayValue == 1);
   }
   
   public static boolean hasCellZeroValue(int [][] grid, int row, int column) {
	   return row < 0 || row >= grid[0].length || column < 0 || column >= grid[0].length || grid[row][column] == 0;
   }
   
   public static void resetSpotValue(int [][] grid, int row, int col) {
   		grid[row][col] = 0;
   }
	
}
