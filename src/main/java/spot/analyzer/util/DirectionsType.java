package spot.analyzer.util;
import java.util.stream.Stream;

public enum DirectionsType implements Moveable {
	
	EAST {
		@Override
		public int[] calculateMove(int row, int column) {
			return new int[] {row + 1, column};
		}
	},
	
	WEST {
		@Override
		public int[] calculateMove(int row, int column) {
			return new int[] {row - 1, column};
		}
	},
	
	SOUTH {
		@Override
		public int[] calculateMove(int row, int column) {
			return new int[] {row, column + 1};
		}
	},
	
	NORTH {
		@Override
		public int[] calculateMove(int row, int column) {
			return new int[] {row, column - 1};
		}
	};
	
	 public static Stream<DirectionsType> stream() {
	        return Stream.of(DirectionsType.values()); 
	 }
}
