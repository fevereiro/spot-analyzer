package spot.analyzer.util;

public interface Moveable {
	
	int [] calculateMove(int row, int column);

}
