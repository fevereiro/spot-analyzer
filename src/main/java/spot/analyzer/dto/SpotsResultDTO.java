package spot.analyzer.dto;

import javax.json.bind.JsonbBuilder;
import javax.json.bind.JsonbConfig;
import javax.json.bind.config.PropertyNamingStrategy;

public class SpotsResultDTO {

	private int totalArea;
	private int numberOfSpots;
	private double averageSpotArea;
	private int biggestSpotArea;
	
	public int getTotalArea() {
		return totalArea;
	}

	public int getNumberOfSpots() {
		return numberOfSpots;
	}

	public double getAverageSpotArea() {
		return averageSpotArea;
	}

	public int getBiggestSpotArea() {
		return biggestSpotArea;
	}
	
	private SpotsResultDTO(Builder resultBuilder) {
		this.averageSpotArea = resultBuilder.averageArea;
		this.biggestSpotArea = resultBuilder.biggestArea;
		this.numberOfSpots = resultBuilder.numberOfSpots;
		this.totalArea = resultBuilder.totalArea;
	}
	
	public String toJson() {
		JsonbConfig jsonbConfig = new JsonbConfig()
				.withPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CASE_WITH_UNDERSCORES);
		return JsonbBuilder.create(jsonbConfig).toJson(this); 
	}
	
	public static class Builder {
		private int totalArea;
		private int numberOfSpots;
		private double averageArea;
		private int biggestArea;
		
		public Builder setTotalArea(int totalArea) {
			this.totalArea = totalArea;
			return this;
		}
		
		public Builder setNumberOfSpots(int numberOfSpots) {
			this.numberOfSpots = numberOfSpots;
			return this;
		}
		
		public Builder setAverageArea(double averageArea) {
			this.averageArea = averageArea;
			return this;
		}
		
		public Builder setBiggestArea(int biggestArea) {
			this.biggestArea = biggestArea;
			return this;
		}

		public SpotsResultDTO build() {
			return new SpotsResultDTO(this);
		}
	}
}
